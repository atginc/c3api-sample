﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atg.c3api.sample
{
    public class JWTPayload
    {
        public JWTPayload()
        {
        }

        public JWTPayload(long iat, string path, string apiKey)
        {
            this.iat = iat;
            this.path = path;
            this.apiKey = apiKey;
        }

        public long iat { get; set; }
        public string path { get; set; }
        public string apiKey { get; set; }
    }
}

﻿using Flurl;
using Flurl.Http;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace atg.c3api.sample
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncContext.Run(() => DoWork());
        }

        private static async void DoWork()
        {
            try
            {
                Console.WriteLine("Enter Your API Key:");
                var apiKey = Console.ReadLine();
                Console.WriteLine();

                Console.WriteLine("Enter Your Signing Key:");
                var hmacKey = Console.ReadLine();
                Console.WriteLine();

                var baseUrl = "https://dzfskc44ri.execute-api.us-west-2.amazonaws.com/Primary/ERP";

                Console.WriteLine("Pulling associations from Branch 601");

                var url = baseUrl.AppendPathSegment("associations").SetQueryParam("branchId", 601).SetQueryParam("take", 1000);

                var uri = new UriBuilder(url).Uri.AbsolutePath;

                var token = GenerateToken(uri, DateTimeOffset.Now.ToUnixTimeSeconds(), apiKey, hmacKey);

                var associations = await url.WithHeader("Authorization", String.Format("Bearer {0}", token)).WithHeader("x-api-key", apiKey).GetJsonAsync<List<C3Association>>();

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("********** CONNECTION SUCCESSFUL **********");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine($"Displaying first 10 associations of {associations.Count()}");
                Console.WriteLine();
                Console.WriteLine();

                foreach (var association in associations.Take(10))
                {
                    Console.WriteLine($"ID: {association.id} | Name: {association.name}");
                }

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine($"There was an error; check your keys and try again. {ex.Message}");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                DoWork();
            }
        }

        private static string GenerateToken(string uri, long iat, string apiKey, string hmacKey)
        {
            uri = uri.Replace("/Primary", "");

            var payload = new JWTPayload(iat, uri, apiKey);

            string jwtToken = Jose.JWT.Encode(payload, Encoding.UTF8.GetBytes(hmacKey), Jose.JwsAlgorithm.HS256);

            return jwtToken;
        }
    }
}
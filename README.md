
# C3 API Sample #

This repo provides a quick and dirty demonstration on how to connect to the C3 API.

## What do I need to run this? ##
All that's required for you to run this demo is the API Key and signing key that ATG provided you when you were granted access to the API sandbox.

## What does it do? ##
This demo requests you enter your keys one at a time, then, using a hard-coded base URL and branch ID, calls the sandbox API and retrieves a list of associations.
The code demonstrates how to take the keys provided, build a JWT, and apply the necessary headers to make a successful call.

## JWT-based API Signing ##
### Purpose ###
Implementation of API request message signing is intended to improve API security in three ways: 

 1. Clients must prove ownership of a private signing key by using it to sign each API request
 2. API request tokens will expire after a given period of time in order to limit replay attack time boundaries
 3. API request tokens will only be valid for a limited scope of API calls in order to limit replay attack surface

Use of the JWT standard for API signing is intended to accomplish:
 - An effective implementation of the aforementioned security goals
 - Adherence to open standards to maximize security and minimize customization
 - Reduction of custom code by reuse of open source JWT libraries
### Assumptions ###
This guide specifies a JWT implementation for signing API requests. It does not include any references to encryption, as it is assumed that transport layer encryption over SSL/TLS is utilized at all times. Transporting sensitive information like api keys over an unencrypted channel should never be done without implementing strong message level encryption, which can be easily achieved using JWT but is outside the scope of this guide. 

JWT is frequently used for representing principle claims that are scoped to an application user. This document is not intended for that scenario. Rather, **the intended application is a server-to-server API relationship** where a secret signing key can be securely kept on both servers. Typically these signing keys should be encrypted at rest, and protected with adequate due diligence. Ownership of the signing key is assumed to be the only security requirement for generating valid tokens which will be accepted by the API server. 
### General Specifications ###
- Sign all requests using HMAC SHA256, and include in that signature a timestamp and a URL path. Utilize the default standard JOSE header indicating this: {"alg": "HS256","typ": "JWT"}
- For the timestamp, use the iat registered claim documented here https://tools.ietf.org/html/rfc7519#section-4.1.6. Per the RFC, the value should be a numeric value containing the number of seconds since the UTC epoch. 
- For transportation of the token as an HTTP Header, prefer using the **Authorization** header using the **Bearer** schema. API Gateway Specifications For the URL path, use a non-registered claim named "path". Set its value to a string containing the URL path only, excluding URI scheme, host, port or querystring. For API Gateway partner identification, use the non-registered claim named "apiKey". Set its value to a string containing the Amazon AWS-generated API key. 
#### External References ####
https://jwt.io/ - an excellent source of most information surrounding JWT, including sample generators and links to libraries for various platforms and languages
https://tools.ietf.org/html/rfc7519 - IETF RFC 7519, documenting the JWT standard